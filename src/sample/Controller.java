package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public DatePicker birthday;
    public RadioButton male;
    public RadioButton female;
    public Button btn;
    public ImageView photo;
    public GridPane main;
    public ComboBox <String> blood;
    public ComboBox <String> city;

    public void doselectbirthday(ActionEvent actionEvent) {
        System.out.println(birthday.getValue());
    }

    public void doselectsex(ActionEvent actionEvent) {
        if (male.isSelected()){
            System.out.println(male.getText());

        }
        else {
            System.out.println(female.getText());
        }
    }

    public void dophoto(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File selectedFile = fileChooser.showOpenDialog(main.getScene().getWindow());
        photo.setImage(new Image(selectedFile.toURI().toString()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A","O","P");
        blood.getItems().addAll(items);

        ObservableList<String> itemsplace = FXCollections.observableArrayList();

        String filename = "/Users/Gina/city.txt";
        String line;
        try{
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line=bufferedReader.readLine())!=null){
                itemsplace.add(line);
            }

            bufferedReader.close();
            city.setItems(itemsplace);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
